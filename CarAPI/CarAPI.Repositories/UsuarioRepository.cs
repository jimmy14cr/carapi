﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}
