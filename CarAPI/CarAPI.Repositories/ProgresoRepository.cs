﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class ProgresoRepository : Repository<Progreso>, IProgresoRepository
    {
        public ProgresoRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}
