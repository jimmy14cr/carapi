﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class RolRepository : Repository<Rol>, IRolRepository
    {
        public RolRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}
