﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class LeccionRepository : Repository<Leccion>, ILeccionRepository
    {
        public LeccionRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}
