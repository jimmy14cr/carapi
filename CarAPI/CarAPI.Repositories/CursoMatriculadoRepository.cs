﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class CursoMatriculadoRepository : Repository<CursoMatriculado>, ICursoMatriculadoRepository
    {
        public CursoMatriculadoRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}