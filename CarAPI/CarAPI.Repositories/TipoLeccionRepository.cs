﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class TipoLeccionRepository : Repository<TipoLeccion>, ITipoLeccionRepository
    {
        public TipoLeccionRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}