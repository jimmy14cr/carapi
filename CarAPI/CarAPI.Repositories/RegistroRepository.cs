﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class RegistroRepository : Repository<Registro>, IRegistroRepository
    {
        public RegistroRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}
