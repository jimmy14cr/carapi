﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class AsigCursoRolRepository : Repository<AsigCursoRol>, IAsigCursoRolRepository
    {
        public AsigCursoRolRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}