﻿using CarAPI.IRepositories;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Repositories
{
    public class DetalleEvaluacionRepository : Repository<DetalleEvaluacion>, IDetalleEvaluacionRepository
    {
        public DetalleEvaluacionRepository(dbcoopealfarorContext context)
            : base(context)
        {
        }
    }
}
