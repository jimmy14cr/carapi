﻿using CarAPI.Infrastructure.Model;
using CarAPI.Model.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IRepositories
{
    public interface ILeccionRepository : IRepository<Leccion>
    {
    }
}

