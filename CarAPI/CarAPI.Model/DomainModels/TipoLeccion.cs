﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class TipoLeccion
    {
        public TipoLeccion()
        {
            Leccions = new HashSet<Leccion>();
        }

        public Guid IdTipoLeccion { get; set; }
        public string Modalidad { get; set; }
        public bool? Enabled { get; set; }

        public virtual ICollection<Leccion> Leccions { get; set; }
    }
}
