﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class CursoMatriculado
    {
        public CursoMatriculado()
        {
            Usuarios = new HashSet<Usuario>();
        }

        public Guid IdCursoMatriculado { get; set; }
        public Guid? IdCurso { get; set; }
        public Guid? IdEvaluacion { get; set; }
        public Guid? IdProgreso { get; set; }
        public bool? Enabled { get; set; }

        public virtual Curso IdCursoNavigation { get; set; }
        public virtual Evaluacion IdEvaluacionNavigation { get; set; }
        public virtual Progreso IdProgresoNavigation { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
