﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class dbcoopealfarorContext : DbContext
    {
        public dbcoopealfarorContext()
        {
        }

        public dbcoopealfarorContext(DbContextOptions<dbcoopealfarorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AsigCursoRol> AsigCursoRols { get; set; }
        public virtual DbSet<Curso> Cursos { get; set; }
        public virtual DbSet<CursoMatriculado> CursoMatriculados { get; set; }
        public virtual DbSet<DetalleEvaluacion> DetalleEvaluacions { get; set; }
        public virtual DbSet<Evaluacion> Evaluacions { get; set; }
        public virtual DbSet<Leccion> Leccions { get; set; }
        public virtual DbSet<Modulo> Modulos { get; set; }
        public virtual DbSet<Progreso> Progresos { get; set; }
        public virtual DbSet<Registro> Registros { get; set; }
        public virtual DbSet<Rol> Rols { get; set; }
        public virtual DbSet<TipoLeccion> TipoLeccions { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AsigCursoRol>(entity =>
            {
                entity.HasKey(x => x.IdAsignacion)
                    .HasName("PK__AsigCurs__A7235DFFF3EBA004");

                entity.ToTable("AsigCursoRol");

                entity.Property(e => e.IdAsignacion).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.IdCursoNavigation)
                    .WithMany(p => p.AsigCursoRols)
                    .HasForeignKey(x => x.IdCurso)
                    .HasConstraintName("FK__AsigCurso__IdCur__534D60F1");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.AsigCursoRols)
                    .HasForeignKey(x => x.IdRol)
                    .HasConstraintName("FK__AsigCurso__Enabl__52593CB8");
            });

            modelBuilder.Entity<Curso>(entity =>
            {
                entity.HasKey(x => x.IdCurso)
                    .HasName("PK__Cursos__085F27D610C5D588");

                entity.ToTable("Curso");

                entity.Property(e => e.IdCurso).HasDefaultValueSql("(newid())");

                entity.Property(e => e.FechaFinal)
                    .HasColumnType("datetime")
                    .HasAnnotation("Relational:ColumnType", "datetime");

                entity.Property(e => e.FechaInicio)
                    .HasColumnType("datetime")
                    .HasAnnotation("Relational:ColumnType", "datetime");

                entity.Property(e => e.NombreCurso)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAsignacionNavigation)
                    .WithMany(p => p.Cursos)
                    .HasForeignKey(x => x.IdAsignacion)
                    .HasConstraintName("FK_Cursos_AsigCursoRol");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.Cursos)
                    .HasForeignKey(x => x.IdModulo)
                    .HasConstraintName("FK__Cursos__Enabled__4E88ABD4");
            });

            modelBuilder.Entity<CursoMatriculado>(entity =>
            {
                entity.HasKey(x => x.IdCursoMatriculado)
                    .HasName("PK__CursosMa__6684C511BFFD6BD8");

                entity.ToTable("CursoMatriculado");

                entity.Property(e => e.IdCursoMatriculado).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.IdCursoNavigation)
                    .WithMany(p => p.CursoMatriculados)
                    .HasForeignKey(x => x.IdCurso)
                    .HasConstraintName("FK__CursosMat__Enabl__5812160E");

                entity.HasOne(d => d.IdEvaluacionNavigation)
                    .WithMany(p => p.CursoMatriculados)
                    .HasForeignKey(x => x.IdEvaluacion)
                    .HasConstraintName("FK__CursosMat__IdEva__59063A47");

                entity.HasOne(d => d.IdProgresoNavigation)
                    .WithMany(p => p.CursoMatriculados)
                    .HasForeignKey(x => x.IdProgreso)
                    .HasConstraintName("FK__CursosMat__IdPro__59FA5E80");
            });

            modelBuilder.Entity<DetalleEvaluacion>(entity =>
            {
                entity.HasKey(x => x.IdDetalle)
                    .HasName("PK__DetalleE__E43646A5628DF63B");

                entity.ToTable("DetalleEvaluacion");

                entity.Property(e => e.IdDetalle).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Evaluacion>(entity =>
            {
                entity.HasKey(x => x.IdEvaluacion)
                    .HasName("PK__Evaluaci__A7EA657CB7691C28");

                entity.ToTable("Evaluacion");

                entity.Property(e => e.IdEvaluacion).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.IdDetalleNavigation)
                    .WithMany(p => p.Evaluacions)
                    .HasForeignKey(x => x.IdDetalle)
                    .HasConstraintName("FK__Evaluacio__Enabl__47DBAE45");
            });

            modelBuilder.Entity<Leccion>(entity =>
            {
                entity.HasKey(x => x.IdLeccion)
                    .HasName("PK__Leccione__885C9BBFB53078DA");

                entity.ToTable("Leccion");

                entity.Property(e => e.IdLeccion).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.Leccions)
                    .HasForeignKey(x => x.IdModulo)
                    .HasConstraintName("FK__Lecciones__Enabl__4316F928");

                entity.HasOne(d => d.IdTipoLeccionNavigation)
                    .WithMany(p => p.Leccions)
                    .HasForeignKey(x => x.IdTipoLeccion)
                    .HasConstraintName("FK__Lecciones__IdTip__440B1D61");
            });

            modelBuilder.Entity<Modulo>(entity =>
            {
                entity.HasKey(x => x.IdModulo)
                    .HasName("PK__Modulo__D9F15315D07A4073");

                entity.ToTable("Modulo");

                entity.Property(e => e.IdModulo).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Progreso>(entity =>
            {
                entity.HasKey(x => x.IdProgreso)
                    .HasName("PK__Progreso__AF525F5B15ADAE46");

                entity.ToTable("Progreso");

                entity.Property(e => e.IdProgreso).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Registro>(entity =>
            {
                entity.HasKey(x => x.IdRegistro)
                    .HasName("PK__Registro__FFA45A993BF7F538");

                entity.ToTable("Registro");

                entity.Property(e => e.IdRegistro).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Identificacion)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.Registros)
                    .HasForeignKey(x => x.IdRol)
                    .HasConstraintName("FK__Registro__Enable__628FA481");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Registros)
                    .HasForeignKey(x => x.IdUsuario)
                    .HasConstraintName("FK__Registro__IdUsua__6383C8BA");
            });

            modelBuilder.Entity<Rol>(entity =>
            {
                entity.HasKey(x => x.IdRol)
                    .HasName("PK__Roles__2A49584C6B6F70B0");

                entity.ToTable("Rol");

                entity.Property(e => e.IdRol).HasDefaultValueSql("(newid())");

                entity.Property(e => e.TipoRol)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TipoLeccion>(entity =>
            {
                entity.HasKey(x => x.IdTipoLeccion)
                    .HasName("PK__TipoLecc__7F74818BB13359A1");

                entity.ToTable("TipoLeccion");

                entity.Property(e => e.IdTipoLeccion).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Modalidad)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(x => x.IdUsuario)
                    .HasName("PK__Usuario__5B65BF974739AB49");

                entity.ToTable("Usuario");

                entity.Property(e => e.IdUsuario).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Apellidos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Puesto)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdCursoMatriculadoNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(x => x.IdCursoMatriculado)
                    .HasConstraintName("FK__Usuario__IdCurso__5EBF139D");

                entity.HasOne(d => d.IdRegistroNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(x => x.IdRegistro)
                    .HasConstraintName("FK_Usuario_Registro");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(x => x.IdRol)
                    .HasConstraintName("FK__Usuario__Enabled__5DCAEF64");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
