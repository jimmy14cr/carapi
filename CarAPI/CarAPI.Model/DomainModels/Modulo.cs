﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class Modulo
    {
        public Modulo()
        {
            Cursos = new HashSet<Curso>();
            Leccions = new HashSet<Leccion>();
        }

        public Guid IdModulo { get; set; }
        public string Descripcion { get; set; }
        public bool? Enabled { get; set; }

        public virtual ICollection<Curso> Cursos { get; set; }
        public virtual ICollection<Leccion> Leccions { get; set; }
    }
}
