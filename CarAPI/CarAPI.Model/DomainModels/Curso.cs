﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class Curso
    {
        public Curso()
        {
            AsigCursoRols = new HashSet<AsigCursoRol>();
            CursoMatriculados = new HashSet<CursoMatriculado>();
        }

        public Guid IdCurso { get; set; }
        public string NombreCurso { get; set; }
        public int? CodigoCurso { get; set; }
        public Guid? IdModulo { get; set; }
        public Guid? IdAsignacion { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFinal { get; set; }
        public bool? Enabled { get; set; }

        public virtual AsigCursoRol IdAsignacionNavigation { get; set; }
        public virtual Modulo IdModuloNavigation { get; set; }
        public virtual ICollection<AsigCursoRol> AsigCursoRols { get; set; }
        public virtual ICollection<CursoMatriculado> CursoMatriculados { get; set; }
    }
}
