﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class Registro
    {
        public Registro()
        {
            Usuarios = new HashSet<Usuario>();
        }

        public Guid IdRegistro { get; set; }
        public string Identificacion { get; set; }
        public string Password { get; set; }
        public Guid? IdRol { get; set; }
        public Guid? IdUsuario { get; set; }
        public bool? Enabled { get; set; }

        public virtual Rol IdRolNavigation { get; set; }
        public virtual Usuario IdUsuarioNavigation { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
