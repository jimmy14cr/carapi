﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class DetalleEvaluacion
    {
        public DetalleEvaluacion()
        {
            Evaluacions = new HashSet<Evaluacion>();
        }

        public Guid IdDetalle { get; set; }
        public string Descripcion { get; set; }
        public bool? Enabled { get; set; }

        public virtual ICollection<Evaluacion> Evaluacions { get; set; }
    }
}
