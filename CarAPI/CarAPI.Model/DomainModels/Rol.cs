﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class Rol
    {
        public Rol()
        {
            AsigCursoRols = new HashSet<AsigCursoRol>();
            Registros = new HashSet<Registro>();
            Usuarios = new HashSet<Usuario>();
        }

        public Guid IdRol { get; set; }
        public string TipoRol { get; set; }
        public bool? Enabled { get; set; }

        public virtual ICollection<AsigCursoRol> AsigCursoRols { get; set; }
        public virtual ICollection<Registro> Registros { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
