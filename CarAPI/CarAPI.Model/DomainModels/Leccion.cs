﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class Leccion
    {
        public Guid IdLeccion { get; set; }
        public Guid? IdModulo { get; set; }
        public string Description { get; set; }
        public Guid? IdTipoLeccion { get; set; }
        public bool? Enabled { get; set; }

        public virtual Modulo IdModuloNavigation { get; set; }
        public virtual TipoLeccion IdTipoLeccionNavigation { get; set; }
    }
}
