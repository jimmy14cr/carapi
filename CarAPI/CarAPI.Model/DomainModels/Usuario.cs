﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class Usuario
    {
        public Usuario()
        {
            Registros = new HashSet<Registro>();
        }

        public Guid IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Puesto { get; set; }
        public Guid? IdRol { get; set; }
        public Guid? IdRegistro { get; set; }
        public Guid? IdCursoMatriculado { get; set; }
        public bool? Enabled { get; set; }

        public virtual CursoMatriculado IdCursoMatriculadoNavigation { get; set; }
        public virtual Registro IdRegistroNavigation { get; set; }
        public virtual Rol IdRolNavigation { get; set; }
        public virtual ICollection<Registro> Registros { get; set; }
    }
}
