﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class AsigCursoRol
    {
        public AsigCursoRol()
        {
            Cursos = new HashSet<Curso>();
        }

        public Guid IdAsignacion { get; set; }
        public Guid? IdRol { get; set; }
        public Guid? IdCurso { get; set; }
        public bool? Enabled { get; set; }

        public virtual Curso IdCursoNavigation { get; set; }
        public virtual Rol IdRolNavigation { get; set; }
        public virtual ICollection<Curso> Cursos { get; set; }
    }
}
