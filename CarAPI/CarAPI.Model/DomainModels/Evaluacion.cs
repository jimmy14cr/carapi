﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class Evaluacion
    {
        public Evaluacion()
        {
            CursoMatriculados = new HashSet<CursoMatriculado>();
        }

        public Guid IdEvaluacion { get; set; }
        public double? ValorObtenido { get; set; }
        public double? Nota { get; set; }
        public double? Promedio { get; set; }
        public Guid? IdDetalle { get; set; }
        public bool? Enabled { get; set; }

        public virtual DetalleEvaluacion IdDetalleNavigation { get; set; }
        public virtual ICollection<CursoMatriculado> CursoMatriculados { get; set; }
    }
}
