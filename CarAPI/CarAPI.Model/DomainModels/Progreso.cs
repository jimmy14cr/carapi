﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarAPI.Model.DomainModels
{
    public partial class Progreso
    {
        public Progreso()
        {
            CursoMatriculados = new HashSet<CursoMatriculado>();
        }

        public Guid IdProgreso { get; set; }
        public string Descripcion { get; set; }
        public bool? Enabled { get; set; }

        public virtual ICollection<CursoMatriculado> CursoMatriculados { get; set; }
    }
}
