﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.CursoMatriculado;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class CursoMatriculadoMapping
    {
        public static IEnumerable<CursoMatriculadoView> ToCursoMatriculadoViewList(this IEnumerable<CursoMatriculado> list, IMapper mapper)
        {
            return mapper.Map<List<CursoMatriculadoView>>(list);
        }

        public static CursoMatriculadoView ToCursoMatriculadoView(this CursoMatriculado model, IMapper mapper)
        {
            return mapper.Map<CursoMatriculadoView>(model);
        }

        public static CursoMatriculado ToCursoMatriculadoModel(this CursoMatriculadoView modelView, IMapper mapper)
        {
            return mapper.Map<CursoMatriculado>(modelView);
        }
    }
}
