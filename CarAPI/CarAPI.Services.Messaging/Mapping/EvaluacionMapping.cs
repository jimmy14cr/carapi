﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.Evaluacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class EvaluacionMapping
    {
        public static IEnumerable<EvaluacionView> ToEvaluacionViewList(this IEnumerable<Evaluacion> list, IMapper mapper)
        {
            return mapper.Map<List<EvaluacionView>>(list);
        }

        public static EvaluacionView ToEvaluacionView(this Evaluacion model, IMapper mapper)
        {
            return mapper.Map<EvaluacionView>(model);
        }

        public static Evaluacion ToEvaluacionModel(this EvaluacionView modelView, IMapper mapper)
        {
            return mapper.Map<Evaluacion>(modelView);
        }
    }
}
