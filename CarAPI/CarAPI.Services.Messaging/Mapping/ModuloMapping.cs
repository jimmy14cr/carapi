﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.Modulo;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class ModuloMapping
    {
        public static IEnumerable<ModuloView> ToModuloViewList(this IEnumerable<Modulo> list, IMapper mapper)
        {
            return mapper.Map<List<ModuloView>>(list);
        }

        public static ModuloView ToModuloView(this Modulo model, IMapper mapper)
        {
            return mapper.Map<ModuloView>(model);
        }

        public static Modulo ToModuloModel(this ModuloView modelView, IMapper mapper)
        {
            return mapper.Map<Modulo>(modelView);
        }
    }
}
