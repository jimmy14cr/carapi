﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.TipoLeccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class TipoLeccionMapping
    {
        public static IEnumerable<TipoLeccionView> ToTipoLeccionViewList(this IEnumerable<TipoLeccion> list, IMapper mapper)
        {
            return mapper.Map<List<TipoLeccionView>>(list);
        }

        public static TipoLeccionView ToTipoLeccionView(this TipoLeccion model, IMapper mapper)
        {
            return mapper.Map<TipoLeccionView>(model);
        }

        public static TipoLeccion ToTipoLeccionModel(this TipoLeccionView modelView, IMapper mapper)
        {
            return mapper.Map<TipoLeccion>(modelView);
        }
    }
}