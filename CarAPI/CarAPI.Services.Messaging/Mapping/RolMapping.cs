﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.Rol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class RolMapping
    {
        public static IEnumerable<RolView> ToRolViewList(this IEnumerable<Rol> list, IMapper mapper)
        {
            return mapper.Map<List<RolView>>(list);
        }

        public static RolView ToRolView(this Rol model, IMapper mapper)
        {
            return mapper.Map<RolView>(model);
        }

        public static Rol ToRolModel(this RolView modelView, IMapper mapper)
        {
            return mapper.Map<Rol>(modelView);
        }
    }
}
