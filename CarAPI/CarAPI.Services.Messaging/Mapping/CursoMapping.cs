﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.Curso;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class CursoMapping
    {
        public static IEnumerable<CursoView> ToCursoViewList(this IEnumerable<Curso> list, IMapper mapper)
        {
            return mapper.Map<List<CursoView>>(list);
        }

        public static CursoView ToCursoView(this Curso model, IMapper mapper)
        {
            return mapper.Map<CursoView>(model);
        }

        public static Curso ToCursoModel(this CursoView modelView, IMapper mapper)
        {
            return mapper.Map<Curso>(modelView);
        }
    }
}
