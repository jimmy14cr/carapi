﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.AsigCursoRol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class AsigCursoRolMapping
    {
        public static IEnumerable<AsigCursoRolView> ToAsigCursoRolViewList(this IEnumerable<AsigCursoRol> list, IMapper mapper)
        {
            return mapper.Map<List<AsigCursoRolView>>(list);
        }

        public static AsigCursoRolView ToAsigCursoRolView(this AsigCursoRol model, IMapper mapper)
        {
            return mapper.Map<AsigCursoRolView>(model);
        }

        public static AsigCursoRol ToAsigCursoRolModel(this AsigCursoRolView modelView, IMapper mapper)
        {
            return mapper.Map<AsigCursoRol>(modelView);
        }
    }
}
