﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.DetalleEvaluacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class DetalleEvaluacionMapping
    {
        public static IEnumerable<DetalleEvaluacionView> ToDetalleEvaluacionViewList(this IEnumerable<DetalleEvaluacion> list, IMapper mapper)
        {
            return mapper.Map<List<DetalleEvaluacionView>>(list);
        }

        public static DetalleEvaluacionView ToDetalleEvaluacionView(this DetalleEvaluacion model, IMapper mapper)
        {
            return mapper.Map<DetalleEvaluacionView>(model);
        }

        public static DetalleEvaluacion ToDetalleEvaluacionModel(this DetalleEvaluacionView modelView, IMapper mapper)
        {
            return mapper.Map<DetalleEvaluacion>(modelView);
        }
    }
}
