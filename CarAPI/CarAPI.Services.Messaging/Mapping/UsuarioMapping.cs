﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.Usuario;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class UsuarioMapping
    {
        public static IEnumerable<UsuarioView> ToUsuarioViewList(this IEnumerable<Usuario> list, IMapper mapper)
        {
            return mapper.Map<List<UsuarioView>>(list);
        }

        public static UsuarioView ToUsuarioView(this Usuario model, IMapper mapper)
        {
            return mapper.Map<UsuarioView>(model);
        }

        public static Usuario ToUsuarioModel(this UsuarioView modelView, IMapper mapper)
        {
            return mapper.Map<Usuario>(modelView);
        }
    }
}
