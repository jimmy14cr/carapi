﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.Leccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class LeccionMapping
    {
        public static IEnumerable<LeccionView> ToLeccionViewList(this IEnumerable<Leccion> list, IMapper mapper)
        {
            return mapper.Map<List<LeccionView>>(list);
        }

        public static LeccionView ToLeccionView(this Leccion model, IMapper mapper)
        {
            return mapper.Map<LeccionView>(model);
        }

        public static Leccion ToLeccionModel(this LeccionView modelView, IMapper mapper)
        {
            return mapper.Map<Leccion>(modelView);
        }
    }
}
