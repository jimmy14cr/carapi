﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.Progreso;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class ProgresoMapping
    {
        public static IEnumerable<ProgresoView> ToProgresoViewList(this IEnumerable<Progreso> list, IMapper mapper)
        {
            return mapper.Map<List<ProgresoView>>(list);
        }

        public static ProgresoView ToProgresoView(this Progreso model, IMapper mapper)
        {
            return mapper.Map<ProgresoView>(model);
        }

        public static Progreso ToProgresoModel(this ProgresoView modelView, IMapper mapper)
        {
            return mapper.Map<Progreso>(modelView);
        }
    }
}
