﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.Registro;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Mapping
{
    public static class RegistroMapping
    {
        public static IEnumerable<RegistroView> ToRegistroViewList(this IEnumerable<Registro> list, IMapper mapper)
        {
            return mapper.Map<List<RegistroView>>(list);
        }

        public static RegistroView ToRegistroView(this Registro model, IMapper mapper)
        {
            return mapper.Map<RegistroView>(model);
        }

        public static Registro ToRegistroModel(this RegistroView modelView, IMapper mapper)
        {
            return mapper.Map<Registro>(modelView);
        }
    }
}