﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.DetalleEvaluacion
{
    public class DetalleEvaluacionGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}
