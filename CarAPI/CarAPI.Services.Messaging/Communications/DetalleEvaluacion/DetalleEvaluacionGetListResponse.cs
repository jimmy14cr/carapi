﻿using CarAPI.Services.Messaging.ViewModels.DetalleEvaluacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.DetalleEvaluacion
{
    public class DetalleEvaluacionGetListResponse
    {
        public IEnumerable<DetalleEvaluacionView> DetalleEvaluaciones { get; set; }
    }
}
