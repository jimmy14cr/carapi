﻿using CarAPI.Services.Messaging.ViewModels.Modulo;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Modulo
{
    public class ModuloAddRequest
    {
        public ModuloView Modulo
        {
            get;
            set;
        }
    }
}
