﻿using CarAPI.Services.Messaging.ViewModels.Modulo;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Modulo
{
    public class ModuloGetListResponse
    {
        public IEnumerable<ModuloView> Modulos { get; set; }
    }
}