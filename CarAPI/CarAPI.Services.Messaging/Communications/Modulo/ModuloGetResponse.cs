﻿using CarAPI.Services.Messaging.ViewModels.Modulo;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Modulo
{
    public class ModuloGetResponse
    {
        public ModuloView Modulo
        {
            get;
            set;
        }
    }
}