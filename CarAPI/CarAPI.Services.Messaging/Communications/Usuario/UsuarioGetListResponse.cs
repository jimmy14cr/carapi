﻿using CarAPI.Services.Messaging.ViewModels.Usuario;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Usuario
{
    public class UsuarioGetListResponse
    {
        public IEnumerable<UsuarioView> Usuarios { get; set; }
    }
}
