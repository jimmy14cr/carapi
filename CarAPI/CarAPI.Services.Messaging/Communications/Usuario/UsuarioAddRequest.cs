﻿using CarAPI.Services.Messaging.ViewModels.Usuario;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Usuario
{
    public class UsuarioAddRequest
    {
        public UsuarioView Usuario
        {
            get;
            set;
        }
    }
}
