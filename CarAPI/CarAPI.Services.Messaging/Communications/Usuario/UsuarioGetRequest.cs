﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Usuario
{
    public class UsuarioGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}
