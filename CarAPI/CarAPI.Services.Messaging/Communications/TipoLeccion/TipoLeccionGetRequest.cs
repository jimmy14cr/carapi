﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.TipoLeccion
{
    public class TipoLeccionGetRequest
    {
        public Guid Id { get; set; }
        public string Modalidad { get; set; }
    }
}