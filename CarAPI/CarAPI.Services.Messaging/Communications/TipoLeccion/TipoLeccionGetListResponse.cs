﻿using CarAPI.Services.Messaging.ViewModels.TipoLeccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.TipoLeccion
{
    public class TipoLeccionGetListResponse
    {
        public IEnumerable<TipoLeccionView> TipoLecciones { get; set; }
    }
}