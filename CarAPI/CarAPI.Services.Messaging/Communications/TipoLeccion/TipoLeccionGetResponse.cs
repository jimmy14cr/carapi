﻿using CarAPI.Services.Messaging.ViewModels.TipoLeccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.TipoLeccion
{
    public class TipoLeccionGetResponse
    {
        public TipoLeccionView TipoLeccion
        {
            get;
            set;
        }
    }
}
