﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.CursoMatriculado
{
    public class CursoMatriculadoGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}
