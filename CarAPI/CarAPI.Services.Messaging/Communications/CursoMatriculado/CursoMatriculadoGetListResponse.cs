﻿using CarAPI.Services.Messaging.ViewModels.CursoMatriculado;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.CursoMatriculado
{
    public class CursoMatriculadoGetListResponse
    {
        public IEnumerable<CursoMatriculadoView> CursoMatriculados { get; set; }
    }
}
