﻿using CarAPI.Services.Messaging.ViewModels.Leccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Leccion
{
    public class LeccionGetResponse
    {
        public LeccionView Leccion
        {
            get;
            set;
        }
    }
}