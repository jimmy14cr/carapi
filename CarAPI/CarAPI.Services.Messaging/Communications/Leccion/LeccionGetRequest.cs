﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Leccion
{
    public class LeccionGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}