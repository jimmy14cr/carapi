﻿using CarAPI.Services.Messaging.ViewModels.Leccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Leccion
{
    public class LeccionGetListResponse
    {
        public IEnumerable<LeccionView> Lecciones { get; set; }
    }
}
