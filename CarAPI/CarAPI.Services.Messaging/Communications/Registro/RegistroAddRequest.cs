﻿using CarAPI.Services.Messaging.ViewModels.Registro;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Registro
{
    public class RegistroAddRequest
    {
        public RegistroView Registro
        {
            get;
            set;
        }
    }
}