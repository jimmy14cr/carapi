﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Registro
{
    public class RegistroGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}
