﻿using CarAPI.Services.Messaging.ViewModels.AsigCursoRol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.AsigCursoRol
{
    public class AsigCursoRolGetResponse
    {
        public AsigCursoRolView AsigCursoRol
        {
            get;
            set;
        }
    }
}

