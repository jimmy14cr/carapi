﻿using CarAPI.Services.Messaging.ViewModels.AsigCursoRol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.AsigCursoRol
{
    public class AsigCursoRolAddRequest
    {
        public AsigCursoRolView AsigCursoRol
        {
            get;
            set;
        }
    }
}