﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.AsigCursoRol
{
    public class AsigCursoRolGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}
