﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Rol
{
    public class RolGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}
