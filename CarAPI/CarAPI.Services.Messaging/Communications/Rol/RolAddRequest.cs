﻿using CarAPI.Services.Messaging.ViewModels.Rol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Rol
{
    public class RolAddRequest
    {
        public RolView Rol
        {
            get;
            set;
        }
    }
}
