﻿using CarAPI.Services.Messaging.ViewModels.Rol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Rol
{
    public class RolGetListResponse
    {
        public IEnumerable<RolView> Roles { get; set; }
    }
}
