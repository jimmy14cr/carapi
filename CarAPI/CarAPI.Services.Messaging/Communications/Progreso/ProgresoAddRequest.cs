﻿using CarAPI.Services.Messaging.ViewModels.Progreso;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Progreso
{
    public class ProgresoAddRequest
    {
        public ProgresoView Progreso
        {
            get;
            set;
        }
    }
}
