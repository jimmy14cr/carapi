﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Progreso
{
    public class ProgresoGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}
