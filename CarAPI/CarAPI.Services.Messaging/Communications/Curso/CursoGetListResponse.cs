﻿using CarAPI.Services.Messaging.ViewModels.Curso;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Curso
{
    public class CursoGetListResponse
    {
        public IEnumerable<CursoView> Cursos { get; set; }
    }
}
