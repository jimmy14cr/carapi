﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Curso
{
    public class CursoGetRequest
    {
        public Guid Id { get; set; }
        public string NombreCurso { get; set; }
    }
}
