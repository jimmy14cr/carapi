﻿using CarAPI.Services.Messaging.ViewModels.Evaluacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Evaluacion
{
    public class EvaluacionGetResponse
    {
        public EvaluacionView Evaluacion
        {
            get;
            set;
        }
    }
}
