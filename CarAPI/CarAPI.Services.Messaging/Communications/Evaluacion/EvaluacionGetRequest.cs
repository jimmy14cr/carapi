﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.Communications.Evaluacion
{
    public class EvaluacionGetRequest
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
    }
}
