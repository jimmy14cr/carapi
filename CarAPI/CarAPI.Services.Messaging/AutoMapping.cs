﻿using AutoMapper;
using CarAPI.Model.DomainModels;
using CarAPI.Services.Messaging.ViewModels.AsigCursoRol;
using CarAPI.Services.Messaging.ViewModels.Curso;
using CarAPI.Services.Messaging.ViewModels.CursoMatriculado;
using CarAPI.Services.Messaging.ViewModels.DetalleEvaluacion;
using CarAPI.Services.Messaging.ViewModels.Evaluacion;
using CarAPI.Services.Messaging.ViewModels.Leccion;
using CarAPI.Services.Messaging.ViewModels.Modulo;
using CarAPI.Services.Messaging.ViewModels.Progreso;
using CarAPI.Services.Messaging.ViewModels.Registro;
using CarAPI.Services.Messaging.ViewModels.Rol;
using CarAPI.Services.Messaging.ViewModels.TipoLeccion;
using CarAPI.Services.Messaging.ViewModels.Usuario;

namespace CarAPI.Services.Messaging
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<AsigCursoRol, AsigCursoRolView>().ReverseMap();
            CreateMap<Curso, CursoView>().ReverseMap();
            CreateMap<CursoMatriculado, CursoMatriculadoView>().ReverseMap();
            CreateMap<DetalleEvaluacion, DetalleEvaluacionView>().ReverseMap();
            CreateMap<Evaluacion, EvaluacionView>().ReverseMap();
            CreateMap<Leccion, LeccionView>().ReverseMap();
            CreateMap<Modulo, ModuloView>().ReverseMap();
            CreateMap<Progreso, ProgresoView>().ReverseMap();
            CreateMap<Registro, RegistroView>().ReverseMap();
            CreateMap<Rol, RolView>().ReverseMap();
            CreateMap<TipoLeccion, TipoLeccionView>().ReverseMap();
            CreateMap<Usuario, UsuarioView>().ReverseMap();
        }
    }
}
