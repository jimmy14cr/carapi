﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.Rol
{
    public class RolView
    {
        public Guid IdRol { get; set; }
        public string TipoRol { get; set; }
        public bool? Enabled { get; set; }
    }
}
