﻿using CarAPI.Services.Messaging.ViewModels.Curso;
using CarAPI.Services.Messaging.ViewModels.Evaluacion;
using CarAPI.Services.Messaging.ViewModels.Progreso;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.CursoMatriculado
{
    public class CursoMatriculadoView
    {
        public Guid IdCursoMatriculado { get; set; }
        public Guid? IdCurso { get; set; }
        public Guid? IdEvaluacion { get; set; }
        public Guid? IdProgreso { get; set; }
        public bool? Enabled { get; set; }

        public CursoView IdCursoNavigation { get; set; }
        public EvaluacionView IdEvaluacionNavigation { get; set; }
        public ProgresoView IdProgresoNavigation { get; set; }
    }
}
