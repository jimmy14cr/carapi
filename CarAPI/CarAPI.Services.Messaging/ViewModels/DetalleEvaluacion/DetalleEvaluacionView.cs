﻿    using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.DetalleEvaluacion
{
    public class DetalleEvaluacionView
    {
        public Guid IdDetalle { get; set; }
        public string Descripcion { get; set; }
        public bool? Enabled { get; set; }
    }
}
