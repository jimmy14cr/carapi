﻿using CarAPI.Services.Messaging.ViewModels.AsigCursoRol;
using CarAPI.Services.Messaging.ViewModels.Modulo;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.Curso
{
    public class CursoView
    {
        public Guid IdCurso { get; set; }
        public string NombreCurso { get; set; }
        public int? CodigoCurso { get; set; }
        public Guid? IdModulo { get; set; }
        public Guid? IdAsignacion { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFinal { get; set; }
        public bool? Enabled { get; set; }

        public AsigCursoRolView IdAsignacionNavigation { get; set; }
        public ModuloView IdModuloNavigation { get; set; }
    }
}
