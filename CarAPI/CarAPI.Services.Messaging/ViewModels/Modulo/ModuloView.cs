﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.Modulo
{
    public class ModuloView
    {
        public Guid IdModulo { get; set; }
        public string Descripcion { get; set; }
        public bool? Enabled { get; set; }
    }
}
