﻿using CarAPI.Services.Messaging.ViewModels.CursoMatriculado;
using CarAPI.Services.Messaging.ViewModels.Registro;
using CarAPI.Services.Messaging.ViewModels.Rol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.Usuario
{
    public class UsuarioView
    {
        public Guid IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Puesto { get; set; }
        public Guid? IdRol { get; set; }
        public Guid? IdRegistro { get; set; }
        public Guid? IdCursoMatriculado { get; set; }
        public bool? Enabled { get; set; }

        public CursoMatriculadoView IdCursoMatriculadoNavigation { get; set; }
        public RegistroView IdRegistroNavigation { get; set; }
        public RolView IdRolNavigation { get; set; }
    }
}
