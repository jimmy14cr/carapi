﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.Progreso
{
    public class ProgresoView
    {
        public Guid IdProgreso { get; set; }
        public string Descripcion { get; set; }
        public bool? Enabled { get; set; }
    }
}

