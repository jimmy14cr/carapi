﻿using CarAPI.Services.Messaging.ViewModels.DetalleEvaluacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.Evaluacion
{
    public class EvaluacionView
    {
        public Guid IdEvaluacion { get; set; }
        public double? ValorObtenido { get; set; }
        public double? Nota { get; set; }
        public double? Promedio { get; set; }
        public Guid? IdDetalle { get; set; }
        public bool? Enabled { get; set; }

        public DetalleEvaluacionView IdDetalleNavigation { get; set; }
    }
}
