﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.TipoLeccion
{
    public class TipoLeccionView
    {
        //public TipoLeccion()
        //{
        //    Lecciones = new HashSet<Leccione>();
        //}

        public Guid IdTipoLeccion { get; set; }
        public string Modalidad { get; set; }
        public bool? Enabled { get; set; }

        //public virtual ICollection<Leccione> Lecciones { get; set; }
    }
}
