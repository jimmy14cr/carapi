﻿using CarAPI.Services.Messaging.ViewModels.Curso;
using CarAPI.Services.Messaging.ViewModels.Rol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.AsigCursoRol
{
    public class AsigCursoRolView
    {
        public Guid IdAsignacion { get; set; }
        public Guid? IdRol { get; set; }
        public Guid? IdCurso { get; set; }
        public bool? Enabled { get; set; }

        public CursoView IdCursoNavigation { get; set; }
        public RolView IdRolNavigation { get; set; }
    }
}
