﻿using CarAPI.Services.Messaging.ViewModels.Modulo;
using CarAPI.Services.Messaging.ViewModels.TipoLeccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.Leccion
{
    public class LeccionView
    {
        public Guid IdLeccion { get; set; }
        public Guid? IdModulo { get; set; }
        public string Description { get; set; }
        public Guid? IdTipoLeccion { get; set; }
        public bool? Enabled { get; set; }

        public ModuloView IdModuloNavigation { get; set; }
        public TipoLeccionView IdTipoLeccionNavigation { get; set; }
    }
}
