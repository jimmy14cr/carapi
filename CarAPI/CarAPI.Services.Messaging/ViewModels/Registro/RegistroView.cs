﻿using CarAPI.Services.Messaging.ViewModels.Rol;
using CarAPI.Services.Messaging.ViewModels.Usuario;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Services.Messaging.ViewModels.Registro
{
    public class RegistroView
    {
        public Guid IdRegistro { get; set; }
        public string Identificacion { get; set; }
        public string Password { get; set; }
        public Guid? IdRol { get; set; }
        public Guid? IdUsuario { get; set; }
        public bool? Enabled { get; set; }

        public RolView IdRolNavigation { get; set; }
        public UsuarioView IdUsuarioNavigation { get; set; }
    }
}
