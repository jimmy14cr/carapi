﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.DetalleEvaluacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface IDetalleEvaluacionService
    {
        ContractResponse<DetalleEvaluacionGetListResponse> GetDetalleEvaluaciones(ContractRequest<DetalleEvaluacionGetRequest> request);
        ContractResponse<DetalleEvaluacionGetResponse> GetDetalleEvaluacion(ContractRequest<DetalleEvaluacionGetRequest> request);
        ContractResponse<DetalleEvaluacionGetResponse> AddDetalleEvaluacion(ContractRequest<DetalleEvaluacionAddRequest> request);
        ContractResponse<DetalleEvaluacionGetResponse> UpdateDetalleEvaluacion(ContractRequest<DetalleEvaluacionAddRequest> request);
    }
}