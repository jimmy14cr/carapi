﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.Usuario;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface IUsuarioService
    {
        ContractResponse<UsuarioGetListResponse> GetUsuarios(ContractRequest<UsuarioGetRequest> request);
        ContractResponse<UsuarioGetResponse> GetUsuario(ContractRequest<UsuarioGetRequest> request);
        ContractResponse<UsuarioGetResponse> AddUsuario(ContractRequest<UsuarioAddRequest> request);
        ContractResponse<UsuarioGetResponse> UpdateUsuario(ContractRequest<UsuarioAddRequest> request);
    }
}