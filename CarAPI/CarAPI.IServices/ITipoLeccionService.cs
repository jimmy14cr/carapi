﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.TipoLeccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface ITipoLeccionService
    {
        ContractResponse<TipoLeccionGetListResponse> GetTipoLecciones(ContractRequest<TipoLeccionGetRequest> request);
        ContractResponse<TipoLeccionGetResponse> GetTipoLeccion(ContractRequest<TipoLeccionGetRequest> request);
        ContractResponse<TipoLeccionGetResponse> AddTipoLeccion(ContractRequest<TipoLeccionAddRequest> request);
        ContractResponse<TipoLeccionGetResponse> UpdateTipoLeccion(ContractRequest<TipoLeccionAddRequest> request);
    }
}