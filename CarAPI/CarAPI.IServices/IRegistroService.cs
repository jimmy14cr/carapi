﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.Registro;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface IRegistroService
    {
        ContractResponse<RegistroGetListResponse> GetRegistros(ContractRequest<RegistroGetRequest> request);
        ContractResponse<RegistroGetResponse> GetRegistro(ContractRequest<RegistroGetRequest> request);
        ContractResponse<RegistroGetResponse> AddRegistro(ContractRequest<RegistroAddRequest> request);
        ContractResponse<RegistroGetResponse> UpdateRegistro(ContractRequest<RegistroAddRequest> request);
    }
}