﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.Progreso;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface IProgresoService
    {
        ContractResponse<ProgresoGetListResponse> GetProgresos(ContractRequest<ProgresoGetRequest> request);
        ContractResponse<ProgresoGetResponse> GetProgreso(ContractRequest<ProgresoGetRequest> request);
        ContractResponse<ProgresoGetResponse> AddProgreso(ContractRequest<ProgresoAddRequest> request);
        ContractResponse<ProgresoGetResponse> UpdateProgreso(ContractRequest<ProgresoAddRequest> request);
    }
}