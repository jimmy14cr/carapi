﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.Curso;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface ICursoService
    {
        ContractResponse<CursoGetListResponse> GetCursos(ContractRequest<CursoGetRequest> request);
        ContractResponse<CursoGetResponse> GetCurso(ContractRequest<CursoGetRequest> request);
        ContractResponse<CursoGetResponse> AddCurso(ContractRequest<CursoAddRequest> request);
        ContractResponse<CursoGetResponse> UpdateCurso(ContractRequest<CursoAddRequest> request);
    }
}
