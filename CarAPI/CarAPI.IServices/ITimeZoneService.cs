﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface ITimeZoneService
    {
        DateTime Now();
    }
}