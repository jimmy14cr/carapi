﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.Modulo;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface IModuloService
    {
        ContractResponse<ModuloGetListResponse> GetModulos(ContractRequest<ModuloGetRequest> request);
        ContractResponse<ModuloGetResponse> GetModulo(ContractRequest<ModuloGetRequest> request);
        ContractResponse<ModuloGetResponse> AddModulo(ContractRequest<ModuloAddRequest> request);
        ContractResponse<ModuloGetResponse> UpdateModulo(ContractRequest<ModuloAddRequest> request);
    }
}