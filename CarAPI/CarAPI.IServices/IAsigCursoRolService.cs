﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.AsigCursoRol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface IAsigCursoRolService
    {
        ContractResponse<AsigCursoRolGetListResponse> GetAsigCursoRoles(ContractRequest<AsigCursoRolGetRequest> request);
        ContractResponse<AsigCursoRolGetResponse> GetAsigCursoRol(ContractRequest<AsigCursoRolGetRequest> request);
        ContractResponse<AsigCursoRolGetResponse> AddAsigCursoRol(ContractRequest<AsigCursoRolAddRequest> request);
        ContractResponse<AsigCursoRolGetResponse> UpdateAsigCursoRol(ContractRequest<AsigCursoRolAddRequest> request);
    }
}
