﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.CursoMatriculado;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface ICursoMatriculadoService
    {
        ContractResponse<CursoMatriculadoGetListResponse> GetCursosMatriculados(ContractRequest<CursoMatriculadoGetRequest> request);
        ContractResponse<CursoMatriculadoGetResponse> GetCursoMatriculado(ContractRequest<CursoMatriculadoGetRequest> request);
        ContractResponse<CursoMatriculadoGetResponse> AddCursoMatriculado(ContractRequest<CursoMatriculadoAddRequest> request);
        ContractResponse<CursoMatriculadoGetResponse> UpdateCursoMatriculado(ContractRequest<CursoMatriculadoAddRequest> request);
    }
}