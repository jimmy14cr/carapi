﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.Evaluacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface IEvaluacionService
    {
        ContractResponse<EvaluacionGetListResponse> GetEvaluaciones(ContractRequest<EvaluacionGetRequest> request);
        ContractResponse<EvaluacionGetResponse> GetEvaluacion(ContractRequest<EvaluacionGetRequest> request);
        ContractResponse<EvaluacionGetResponse> AddEvaluacion(ContractRequest<EvaluacionAddRequest> request);
        ContractResponse<EvaluacionGetResponse> UpdateEvaluacion(ContractRequest<EvaluacionAddRequest> request);
    }
}