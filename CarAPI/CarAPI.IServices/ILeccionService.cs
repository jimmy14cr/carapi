﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.Leccion;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface ILeccionService
    {
        ContractResponse<LeccionGetListResponse> GetLecciones(ContractRequest<LeccionGetRequest> request);
        ContractResponse<LeccionGetResponse> GetLeccion(ContractRequest<LeccionGetRequest> request);
        ContractResponse<LeccionGetResponse> AddLeccion(ContractRequest<LeccionAddRequest> request);
        ContractResponse<LeccionGetResponse> UpdateLeccion(ContractRequest<LeccionAddRequest> request);
    }
}
