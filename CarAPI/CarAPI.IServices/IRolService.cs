﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.Services.Messaging.Communications.Rol;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.IServices
{
    public interface IRolService
    {
        ContractResponse<RolGetListResponse> GetRoles(ContractRequest<RolGetRequest> request);
        ContractResponse<RolGetResponse> GetRol(ContractRequest<RolGetRequest> request);
        ContractResponse<RolGetResponse> AddRol(ContractRequest<RolAddRequest> request);
        ContractResponse<RolGetResponse> UpdateRol(ContractRequest<RolAddRequest> request);
    }
}