﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Curso;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/curso")]
    public class CursoController : Controller
    {
        private readonly ICursoService _cursoService;
        private readonly ILogger<CursoController> _logger;

        public CursoController(ICursoService cursoService, ILogger<CursoController> logger)
        {
            _cursoService = cursoService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var CursosResponse = _cursoService.GetCursos(new ContractRequest<CursoGetRequest>());
            return Json(CursosResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _cursoService.GetCurso(new ContractRequest<CursoGetRequest> { Data = new CursoGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<CursoAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _cursoService.AddCurso(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<CursoAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _cursoService.UpdateCurso(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}