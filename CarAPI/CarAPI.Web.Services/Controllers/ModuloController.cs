﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Modulo;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/modulo")]
    public class ModuloController : Controller
    {
        private readonly IModuloService _moduloService;
        private readonly ILogger<ModuloController> _logger;

        public ModuloController(IModuloService moduloService, ILogger<ModuloController> logger)
        {
            _moduloService = moduloService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var ModulosResponse = _moduloService.GetModulos(new ContractRequest<ModuloGetRequest>());
            return Json(ModulosResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _moduloService.GetModulo(new ContractRequest<ModuloGetRequest> { Data = new ModuloGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<ModuloAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _moduloService.AddModulo(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<ModuloAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _moduloService.UpdateModulo(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}
