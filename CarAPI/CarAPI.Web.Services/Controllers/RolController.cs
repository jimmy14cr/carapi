﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Rol;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/rol")]
    public class RolController : Controller
    {
        private readonly IRolService _rolService;
        private readonly ILogger<RolController> _logger;

        public RolController(IRolService rolService, ILogger<RolController> logger)
        {
            _rolService = rolService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var RolsResponse = _rolService.GetRoles(new ContractRequest<RolGetRequest>());
            return Json(RolsResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _rolService.GetRol(new ContractRequest<RolGetRequest> { Data = new RolGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<RolAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _rolService.AddRol(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<RolAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _rolService.UpdateRol(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}
