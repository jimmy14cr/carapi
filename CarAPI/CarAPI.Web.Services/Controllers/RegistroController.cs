﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Registro;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/registro")]
    public class RegistroController : Controller
    {
        private readonly IRegistroService _registroService;
        private readonly ILogger<RegistroController> _logger;

        public RegistroController(IRegistroService registroService, ILogger<RegistroController> logger)
        {
            _registroService = registroService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var RegistrosResponse = _registroService.GetRegistros(new ContractRequest<RegistroGetRequest>());
            return Json(RegistrosResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _registroService.GetRegistro(new ContractRequest<RegistroGetRequest> { Data = new RegistroGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<RegistroAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _registroService.AddRegistro(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<RegistroAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _registroService.UpdateRegistro(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}
