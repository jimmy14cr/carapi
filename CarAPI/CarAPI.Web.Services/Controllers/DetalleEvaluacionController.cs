﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.DetalleEvaluacion;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/detalleEvaluacion")]
    public class DetalleEvaluacionController : Controller
    {
        private readonly IDetalleEvaluacionService _detalleEvaluacionService;
        private readonly ILogger<DetalleEvaluacionController> _logger;

        public DetalleEvaluacionController(IDetalleEvaluacionService detalleEvaluacionService, ILogger<DetalleEvaluacionController> logger)
        {
            _detalleEvaluacionService = detalleEvaluacionService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var DetalleEvaluacionsResponse = _detalleEvaluacionService.GetDetalleEvaluaciones(new ContractRequest<DetalleEvaluacionGetRequest>());
            return Json(DetalleEvaluacionsResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _detalleEvaluacionService.GetDetalleEvaluacion(new ContractRequest<DetalleEvaluacionGetRequest> { Data = new DetalleEvaluacionGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<DetalleEvaluacionAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _detalleEvaluacionService.AddDetalleEvaluacion(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<DetalleEvaluacionAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _detalleEvaluacionService.UpdateDetalleEvaluacion(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}
