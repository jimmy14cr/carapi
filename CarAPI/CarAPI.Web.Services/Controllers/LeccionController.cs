﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Leccion;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/leccion")]
    public class LeccionController : Controller
    {
        private readonly ILeccionService _leccionService;
        private readonly ILogger<LeccionController> _logger;

        public LeccionController(ILeccionService leccionService, ILogger<LeccionController> logger)
        {
            _leccionService = leccionService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var LeccionsResponse = _leccionService.GetLecciones(new ContractRequest<LeccionGetRequest>());
            return Json(LeccionsResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _leccionService.GetLeccion(new ContractRequest<LeccionGetRequest> { Data = new LeccionGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<LeccionAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _leccionService.AddLeccion(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<LeccionAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _leccionService.UpdateLeccion(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}
