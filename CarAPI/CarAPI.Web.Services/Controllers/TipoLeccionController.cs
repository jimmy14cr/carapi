﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.TipoLeccion;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/tipoLeccion")]
    public class TipoLeccionController : Controller
    {
        private readonly ITipoLeccionService _tipoLeccionService;
        private readonly ILogger<TipoLeccionController> _logger;

        public TipoLeccionController(ITipoLeccionService tipoLeccionService, ILogger<TipoLeccionController> logger)
        {
            _tipoLeccionService = tipoLeccionService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var tipoLeccionsResponse = _tipoLeccionService.GetTipoLecciones(new ContractRequest<TipoLeccionGetRequest>());
            return Json(tipoLeccionsResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _tipoLeccionService.GetTipoLeccion(new ContractRequest<TipoLeccionGetRequest> { Data = new TipoLeccionGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<TipoLeccionAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _tipoLeccionService.AddTipoLeccion(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<TipoLeccionAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _tipoLeccionService.UpdateTipoLeccion(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}