﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.AsigCursoRol;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/asigCursoRol")]
    public class AsigCursoRolController : Controller
    {
        private readonly IAsigCursoRolService _asigCursoRolService;
        private readonly ILogger<AsigCursoRolController> _logger;

        public AsigCursoRolController(IAsigCursoRolService asigCursoRolService, ILogger<AsigCursoRolController> logger)
        {
            _asigCursoRolService = asigCursoRolService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var AsigCursoRolsResponse = _asigCursoRolService.GetAsigCursoRoles(new ContractRequest<AsigCursoRolGetRequest>());
            return Json(AsigCursoRolsResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _asigCursoRolService.GetAsigCursoRol(new ContractRequest<AsigCursoRolGetRequest> { Data = new AsigCursoRolGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<AsigCursoRolAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _asigCursoRolService.AddAsigCursoRol(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<AsigCursoRolAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _asigCursoRolService.UpdateAsigCursoRol(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}