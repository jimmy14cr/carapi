﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.CursoMatriculado;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/cursoMatriculado")]
    public class CursoMatriculadoController : Controller
    {
        private readonly ICursoMatriculadoService _cursoMatriculadoService;
        private readonly ILogger<CursoMatriculadoController> _logger;

        public CursoMatriculadoController(ICursoMatriculadoService cursoMatriculadoService, ILogger<CursoMatriculadoController> logger)
        {
            _cursoMatriculadoService = cursoMatriculadoService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var CursoMatriculadosResponse = _cursoMatriculadoService.GetCursosMatriculados(new ContractRequest<CursoMatriculadoGetRequest>());
            return Json(CursoMatriculadosResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _cursoMatriculadoService.GetCursoMatriculado(new ContractRequest<CursoMatriculadoGetRequest> { Data = new CursoMatriculadoGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<CursoMatriculadoAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _cursoMatriculadoService.AddCursoMatriculado(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<CursoMatriculadoAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _cursoMatriculadoService.UpdateCursoMatriculado(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}