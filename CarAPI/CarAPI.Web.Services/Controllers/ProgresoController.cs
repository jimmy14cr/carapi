﻿using CarAPI.Infrastructure.Messaging;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Progreso;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarAPI.Web.Services.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/progreso")]
    public class ProgresoController : Controller
    {
        private readonly IProgresoService _progresoService;
        private readonly ILogger<ProgresoController> _logger;

        public ProgresoController(IProgresoService progresoService, ILogger<ProgresoController> logger)
        {
            _progresoService = progresoService;
            _logger = logger;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var ProgresosResponse = _progresoService.GetProgresos(new ContractRequest<ProgresoGetRequest>());
            return Json(ProgresosResponse);
        }

        [Route("GetById")]
        [HttpGet]
        public JsonResult Get([FromQuery(Name = "id")] Guid id)
        {
            var response = _progresoService.GetProgreso(new ContractRequest<ProgresoGetRequest> { Data = new ProgresoGetRequest { Id = id } });
            return Json(response);
        }

        //[Route("GetByVerify")]
        //[HttpGet]
        //public JsonResult GetByVerify([FromQuery(Name = "departureDate")] DateTime departureDate, [FromQuery(Name = "arriveDate")] DateTime arriveDate)
        //{
        //    var response = _carService.GetCarsByVerify(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { DepartureDate = departureDate, ArriveDate = arriveDate } });
        //    return Json(response);
        //}

        [HttpPost]
        public IActionResult Add([FromBody] ContractRequest<ProgresoAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _progresoService.AddProgreso(request);
            return Json(response);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ContractRequest<ProgresoAddRequest> request)
        {
            if (request?.Data == null)
            {
                return BadRequest();
            }

            var response = _progresoService.UpdateProgreso(request);
            return Json(response);
        }

        //[HttpDelete]
        //public JsonResult Delete([FromQuery(Name = "id")] Guid id)
        //{
        //    var response = _carService.DeleteCar(new ContractRequest<CarGetRequest> { Data = new CarGetRequest { Id = id } });
        //    return Json(response);
        //}

    }
}