using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.Model;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Model.DomainModels;
using CarAPI.Repositories;
using CarAPI.Services;
using CarAPI.Services.Messaging;
//initial commit
namespace CarAPI.Web.Services
{
    //Test
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(c => c.AddProfile<AutoMapping>(), typeof(Startup));
            services.AddDbContext<dbcoopealfarorContext>(options => options.UseSqlServer(Configuration.GetConnectionString("defaultConnection")));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddScoped(typeof(ITimeZoneService), typeof(TimeZoneService));
            services.AddScoped(typeof(IAsigCursoRolRepository), typeof(AsigCursoRolRepository));
            services.AddScoped(typeof(ICursoRepository), typeof(CursoRepository));
            services.AddScoped(typeof(ICursoMatriculadoRepository), typeof(CursoMatriculadoRepository));
            services.AddScoped(typeof(IDetalleEvaluacionRepository), typeof(DetalleEvaluacionRepository));
            services.AddScoped(typeof(IEvaluacionRepository), typeof(EvaluacionRepository));
            services.AddScoped(typeof(ILeccionRepository), typeof(LeccionRepository));
            services.AddScoped(typeof(IModuloRepository), typeof(ModuloRepository));
            services.AddScoped(typeof(IProgresoRepository), typeof(ProgresoRepository));
            services.AddScoped(typeof(IRegistroRepository), typeof(RegistroRepository));
            services.AddScoped(typeof(IRolRepository), typeof(RolRepository));
            services.AddScoped(typeof(ITipoLeccionRepository), typeof(TipoLeccionRepository));
            services.AddScoped(typeof(IUsuarioRepository), typeof(UsuarioRepository));

            services.Configure<AppSettingsModel>(Configuration);

            services.AddScoped(typeof(IAsigCursoRolService), typeof(AsigCursoRolService));
            services.AddScoped(typeof(ICursoService), typeof(CursoService));
            services.AddScoped(typeof(ICursoMatriculadoService), typeof(CursoMatriculadoService));
            services.AddScoped(typeof(IDetalleEvaluacionService), typeof(DetalleEvaluacionService));
            services.AddScoped(typeof(IEvaluacionService), typeof(EvaluacionService));
            services.AddScoped(typeof(ILeccionService), typeof(LeccionService));
            services.AddScoped(typeof(IModuloService), typeof(ModuloService));
            services.AddScoped(typeof(IProgresoService), typeof(ProgresoService));
            services.AddScoped(typeof(IRegistroService), typeof(RegistroService));
            services.AddScoped(typeof(IRolService), typeof(RolService));
            services.AddScoped(typeof(ITipoLeccionService), typeof(TipoLeccionService));
            services.AddScoped(typeof(IUsuarioService), typeof(UsuarioService));

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
