﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Infrastructure.Messaging
{
    public class AppSettingsModel
    {
        public AppSettings AppSettings
        {
            get;
            set;
        }
    }

    public class AppSettings
    {
        public string SendGridKey
        {
            get;
            set;
        }

        public string EmailFrom
        {
            get;
            set;
        }

        public string Subject
        {
            get;
            set;
        }
    }
}
