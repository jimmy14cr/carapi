﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Infrastructure.Model
{
    public class MailConfigSection
    {
        public string From { get; set; }
        public string To { get; set; }
        public string MailgunKey { get; set; }
        public string Domain { get; set; }
    }
}
