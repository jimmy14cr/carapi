﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAPI.Infrastructure.Enums
{
    public class Constants
    {
        public enum Role {
            Administrator = 1,
            Customer = 2
        }

        public enum StatusReservation
        {
            [StringValue("Cancelled")]
            Cancelled = 1,
            [StringValue("Accepted")]
            Accepted = 2,
            [StringValue("Pending")]
            Pending = 3
        }
    }
}
