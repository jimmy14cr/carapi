﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Progreso;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class ProgresoService : IProgresoService
    {
        private readonly IProgresoRepository _progresoRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<ProgresoService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public ProgresoService(IProgresoRepository progresoRepository, ILogger<ProgresoService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _progresoRepository = progresoRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<ProgresoGetResponse> AddProgreso(ContractRequest<ProgresoAddRequest> request)
        {
            ContractResponse<ProgresoGetResponse> response;

            try
            {
                var model = request.Data.Progreso.ToProgresoModel(_mapper);
                model.IdProgreso = Guid.NewGuid();

                var existsProgreso = _progresoRepository.First(u => u.Descripcion == model.Descripcion);

                if (existsProgreso != null)
                    return ContractUtil.CreateInvalidResponse(new ProgresoGetResponse { Progreso = request.Data.Progreso },
                        $"La descripción: {request.Data.Progreso.Descripcion} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<ProgresoGetResponse>(brokenRules);
                }
                else
                {
                    _progresoRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new ProgresoGetResponse
                    {
                        Progreso = model.ToProgresoView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<ProgresoGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<ProgresoGetResponse> GetProgreso(ContractRequest<ProgresoGetRequest> request)
        {
            ContractResponse<ProgresoGetResponse> response;
            try
            {
                var model = _progresoRepository.First(u => u.IdProgreso == request.Data.Id);
                var modelListResponse = new ProgresoGetResponse
                {
                    Progreso = model.ToProgresoView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<ProgresoGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<ProgresoGetListResponse> GetProgresos(ContractRequest<ProgresoGetRequest> request)
        {
            ContractResponse<ProgresoGetListResponse> response;
            try
            {
                var models = _progresoRepository.GetAll();
                var modelListResponse = new ProgresoGetListResponse
                {
                    Progresos = models.ToProgresoViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<ProgresoGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<ProgresoGetResponse> UpdateProgreso(ContractRequest<ProgresoAddRequest> request)
        {
            var existsProgreso = _progresoRepository.First(u => u.Descripcion == request.Data.Progreso.Descripcion && u.IdProgreso != request.Data.Progreso.IdProgreso);
            if (existsProgreso != null)
                return ContractUtil.CreateInvalidResponse(new ProgresoGetResponse { Progreso = request.Data.Progreso },
                        $"La descripción: {request.Data.Progreso.Descripcion} ya existe.");
            _uow.Commit();

            var model = request.Data.Progreso.ToProgresoModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _progresoRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new ProgresoGetResponse
                {
                    Progreso = model.ToProgresoView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<ProgresoGetResponse>(brokenRules);
        }
    }
}
