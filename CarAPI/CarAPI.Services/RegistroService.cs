﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Registro;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class RegistroService : IRegistroService
    {
        private readonly IRegistroRepository _registroRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<RegistroService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public RegistroService(IRegistroRepository registroRepository, ILogger<RegistroService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _registroRepository = registroRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<RegistroGetResponse> AddRegistro(ContractRequest<RegistroAddRequest> request)
        {
            ContractResponse<RegistroGetResponse> response;

            try
            {
                var model = request.Data.Registro.ToRegistroModel(_mapper);
                model.IdRegistro = Guid.NewGuid();

                var existsRegistro = _registroRepository.First(u => u.IdRegistro == model.IdRegistro);

                if (existsRegistro != null)
                    return ContractUtil.CreateInvalidResponse(new RegistroGetResponse { Registro = request.Data.Registro },
                        $"El ID: {request.Data.Registro.IdRegistro} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<RegistroGetResponse>(brokenRules);
                }
                else
                {
                    _registroRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new RegistroGetResponse
                    {
                        Registro = model.ToRegistroView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<RegistroGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<RegistroGetResponse> GetRegistro(ContractRequest<RegistroGetRequest> request)
        {
            ContractResponse<RegistroGetResponse> response;
            try
            {
                var model = _registroRepository.First(u => u.IdRegistro == request.Data.Id);
                var modelListResponse = new RegistroGetResponse
                {
                    Registro = model.ToRegistroView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<RegistroGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<RegistroGetListResponse> GetRegistros(ContractRequest<RegistroGetRequest> request)
        {
            ContractResponse<RegistroGetListResponse> response;
            try
            {
                var models = _registroRepository.GetAll();
                var modelListResponse = new RegistroGetListResponse
                {
                    Registros = models.ToRegistroViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<RegistroGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<RegistroGetResponse> UpdateRegistro(ContractRequest<RegistroAddRequest> request)
        {
            var existsRegistro = _registroRepository.First(u => u.IdRegistro == request.Data.Registro.IdRegistro);
            if (existsRegistro != null)
                return ContractUtil.CreateInvalidResponse(new RegistroGetResponse { Registro = request.Data.Registro },
                        $"El ID: {request.Data.Registro.IdRegistro} ya existe.");
            _uow.Commit();

            var model = request.Data.Registro.ToRegistroModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _registroRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new RegistroGetResponse
                {
                    Registro = model.ToRegistroView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<RegistroGetResponse>(brokenRules);
        }
    }
}