﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.CursoMatriculado;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class CursoMatriculadoService : ICursoMatriculadoService
    {
        private readonly ICursoMatriculadoRepository _cursoMatriculadoRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<CursoMatriculadoService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public CursoMatriculadoService(ICursoMatriculadoRepository cursoMatriculadoRepository, ILogger<CursoMatriculadoService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _cursoMatriculadoRepository = cursoMatriculadoRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<CursoMatriculadoGetResponse> AddCursoMatriculado(ContractRequest<CursoMatriculadoAddRequest> request)
        {
            ContractResponse<CursoMatriculadoGetResponse> response;

            try
            {
                var model = request.Data.CursoMatriculado.ToCursoMatriculadoModel(_mapper);
                model.IdCursoMatriculado = Guid.NewGuid();

                var existsCursoMatriculado = _cursoMatriculadoRepository.First(u => u.IdCursoMatriculado == model.IdCursoMatriculado);

                if (existsCursoMatriculado != null)
                    return ContractUtil.CreateInvalidResponse(new CursoMatriculadoGetResponse { CursoMatriculado = request.Data.CursoMatriculado },
                        $"El curso matriculado: {request.Data.CursoMatriculado.IdCursoMatriculado} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<CursoMatriculadoGetResponse>(brokenRules);
                }
                else
                {
                    _cursoMatriculadoRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new CursoMatriculadoGetResponse
                    {
                        CursoMatriculado = model.ToCursoMatriculadoView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<CursoMatriculadoGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<CursoMatriculadoGetResponse> GetCursoMatriculado(ContractRequest<CursoMatriculadoGetRequest> request)
        {
            ContractResponse<CursoMatriculadoGetResponse> response;
            try
            {
                var model = _cursoMatriculadoRepository.First(u => u.IdCursoMatriculado == request.Data.Id);
                var modelListResponse = new CursoMatriculadoGetResponse
                {
                    CursoMatriculado = model.ToCursoMatriculadoView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<CursoMatriculadoGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<CursoMatriculadoGetListResponse> GetCursosMatriculados(ContractRequest<CursoMatriculadoGetRequest> request)
        {
            ContractResponse<CursoMatriculadoGetListResponse> response;
            try
            {
                var models = _cursoMatriculadoRepository.GetAll();
                var modelListResponse = new CursoMatriculadoGetListResponse
                {
                    CursoMatriculados = models.ToCursoMatriculadoViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<CursoMatriculadoGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<CursoMatriculadoGetResponse> UpdateCursoMatriculado(ContractRequest<CursoMatriculadoAddRequest> request)
        {
            var existsCursoMatriculado = _cursoMatriculadoRepository.First(u => u.IdCursoMatriculado == request.Data.CursoMatriculado.IdCursoMatriculado);
            if (existsCursoMatriculado != null)
                return ContractUtil.CreateInvalidResponse(new CursoMatriculadoGetResponse { CursoMatriculado = request.Data.CursoMatriculado },
                        $"El curso matriculado: {request.Data.CursoMatriculado.IdCursoMatriculado} ya existe.");
            _uow.Commit();

            var model = request.Data.CursoMatriculado.ToCursoMatriculadoModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _cursoMatriculadoRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new CursoMatriculadoGetResponse
                {
                    CursoMatriculado = model.ToCursoMatriculadoView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<CursoMatriculadoGetResponse>(brokenRules);
        }
    }
}