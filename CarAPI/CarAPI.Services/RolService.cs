﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Rol;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class RolService : IRolService
    {
        private readonly IRolRepository _rolRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<RolService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public RolService(IRolRepository rolRepository, ILogger<RolService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _rolRepository = rolRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<RolGetResponse> AddRol(ContractRequest<RolAddRequest> request)
        {
            ContractResponse<RolGetResponse> response;

            try
            {
                var model = request.Data.Rol.ToRolModel(_mapper);
                model.IdRol = Guid.NewGuid();

                var existsRol = _rolRepository.First(u => u.TipoRol == model.TipoRol);

                if (existsRol != null)
                    return ContractUtil.CreateInvalidResponse(new RolGetResponse { Rol = request.Data.Rol },
                        $"El tipo: {request.Data.Rol.TipoRol} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<RolGetResponse>(brokenRules);
                }
                else
                {
                    _rolRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new RolGetResponse
                    {
                        Rol = model.ToRolView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<RolGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<RolGetResponse> GetRol(ContractRequest<RolGetRequest> request)
        {
            ContractResponse<RolGetResponse> response;
            try
            {
                var model = _rolRepository.First(u => u.IdRol == request.Data.Id);
                var modelListResponse = new RolGetResponse
                {
                    Rol = model.ToRolView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<RolGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<RolGetListResponse> GetRoles(ContractRequest<RolGetRequest> request)
        {
            ContractResponse<RolGetListResponse> response;
            try
            {
                var models = _rolRepository.GetAll();
                var modelListResponse = new RolGetListResponse
                {
                    Roles = models.ToRolViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<RolGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<RolGetResponse> UpdateRol(ContractRequest<RolAddRequest> request)
        {
            var existsRol = _rolRepository.First(u => u.TipoRol == request.Data.Rol.TipoRol && u.IdRol != request.Data.Rol.IdRol);
            if (existsRol != null)
                return ContractUtil.CreateInvalidResponse(new RolGetResponse { Rol = request.Data.Rol },
                        $"El tipo: {request.Data.Rol.TipoRol} ya existe.");
            _uow.Commit();

            var model = request.Data.Rol.ToRolModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _rolRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new RolGetResponse
                {
                    Rol = model.ToRolView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<RolGetResponse>(brokenRules);
        }
    }
}