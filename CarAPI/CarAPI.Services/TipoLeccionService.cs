﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.TipoLeccion;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class TipoLeccionService : ITipoLeccionService
    {
        private readonly ITipoLeccionRepository _tipoLeccionRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<TipoLeccionService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public TipoLeccionService(ITipoLeccionRepository tipoLeccionRepository, ILogger<TipoLeccionService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _tipoLeccionRepository = tipoLeccionRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<TipoLeccionGetResponse> AddTipoLeccion(ContractRequest<TipoLeccionAddRequest> request)
        {
            ContractResponse<TipoLeccionGetResponse> response;

            try
            {
                var model = request.Data.TipoLeccion.ToTipoLeccionModel(_mapper);
                model.IdTipoLeccion = Guid.NewGuid();

                var existsTipoLeccion = _tipoLeccionRepository.First(u => u.Modalidad == model.Modalidad);

                if (existsTipoLeccion != null)
                    return ContractUtil.CreateInvalidResponse(new TipoLeccionGetResponse { TipoLeccion = request.Data.TipoLeccion },
                        $"La Modalidad: {request.Data.TipoLeccion.Modalidad} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<TipoLeccionGetResponse>(brokenRules);
                }
                else
                {
                    _tipoLeccionRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new TipoLeccionGetResponse
                    {
                        TipoLeccion = model.ToTipoLeccionView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<TipoLeccionGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<TipoLeccionGetResponse> GetTipoLeccion(ContractRequest<TipoLeccionGetRequest> request)
        {
            ContractResponse<TipoLeccionGetResponse> response;
            try
            {
                var model = _tipoLeccionRepository.First(u => u.IdTipoLeccion == request.Data.Id);
                var modelListResponse = new TipoLeccionGetResponse
                {
                    TipoLeccion = model.ToTipoLeccionView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<TipoLeccionGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<TipoLeccionGetListResponse> GetTipoLecciones(ContractRequest<TipoLeccionGetRequest> request)
        {
            ContractResponse<TipoLeccionGetListResponse> response;
            try
            {
                var models = _tipoLeccionRepository.GetAll();
                var modelListResponse = new TipoLeccionGetListResponse
                {
                    TipoLecciones = models.ToTipoLeccionViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<TipoLeccionGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<TipoLeccionGetResponse> UpdateTipoLeccion(ContractRequest<TipoLeccionAddRequest> request)
        {
            var existsTipoLeccion = _tipoLeccionRepository.First(u => u.Modalidad == request.Data.TipoLeccion.Modalidad && u.IdTipoLeccion != request.Data.TipoLeccion.IdTipoLeccion);
            if (existsTipoLeccion != null)
                return ContractUtil.CreateInvalidResponse(new TipoLeccionGetResponse { TipoLeccion = request.Data.TipoLeccion },
                        $"La modalidad: {request.Data.TipoLeccion.Modalidad} ya existe.");
            _uow.Commit();

            var model = request.Data.TipoLeccion.ToTipoLeccionModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _tipoLeccionRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new TipoLeccionGetResponse
                {
                    TipoLeccion = model.ToTipoLeccionView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<TipoLeccionGetResponse>(brokenRules);
        }
    }
}