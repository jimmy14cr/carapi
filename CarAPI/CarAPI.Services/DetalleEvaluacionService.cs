﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.DetalleEvaluacion;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class DetalleEvaluacionService : IDetalleEvaluacionService
    {
        private readonly IDetalleEvaluacionRepository _detalleEvaluacionRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<DetalleEvaluacionService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public DetalleEvaluacionService(IDetalleEvaluacionRepository detalleEvaluacionRepository, ILogger<DetalleEvaluacionService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _detalleEvaluacionRepository = detalleEvaluacionRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<DetalleEvaluacionGetResponse> AddDetalleEvaluacion(ContractRequest<DetalleEvaluacionAddRequest> request)
        {
            ContractResponse<DetalleEvaluacionGetResponse> response;

            try
            {
                var model = request.Data.DetalleEvaluacion.ToDetalleEvaluacionModel(_mapper);
                model.IdDetalle = Guid.NewGuid();

                var existsDetalleEvaluacion = _detalleEvaluacionRepository.First(u => u.Descripcion == model.Descripcion);

                if (existsDetalleEvaluacion != null)
                    return ContractUtil.CreateInvalidResponse(new DetalleEvaluacionGetResponse { DetalleEvaluacion = request.Data.DetalleEvaluacion },
                        $"La descripción: {request.Data.DetalleEvaluacion.Descripcion} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<DetalleEvaluacionGetResponse>(brokenRules);
                }
                else
                {
                    _detalleEvaluacionRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new DetalleEvaluacionGetResponse
                    {
                        DetalleEvaluacion = model.ToDetalleEvaluacionView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<DetalleEvaluacionGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<DetalleEvaluacionGetResponse> GetDetalleEvaluacion(ContractRequest<DetalleEvaluacionGetRequest> request)
        {
            ContractResponse<DetalleEvaluacionGetResponse> response;
            try
            {
                var model = _detalleEvaluacionRepository.First(u => u.IdDetalle == request.Data.Id);
                var modelListResponse = new DetalleEvaluacionGetResponse
                {
                    DetalleEvaluacion = model.ToDetalleEvaluacionView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<DetalleEvaluacionGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<DetalleEvaluacionGetListResponse> GetDetalleEvaluaciones(ContractRequest<DetalleEvaluacionGetRequest> request)
        {
            ContractResponse<DetalleEvaluacionGetListResponse> response;
            try
            {
                var models = _detalleEvaluacionRepository.GetAll();
                var modelListResponse = new DetalleEvaluacionGetListResponse
                {
                    DetalleEvaluaciones = models.ToDetalleEvaluacionViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<DetalleEvaluacionGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<DetalleEvaluacionGetResponse> UpdateDetalleEvaluacion(ContractRequest<DetalleEvaluacionAddRequest> request)
        {
            var existsDetalleEvaluacion = _detalleEvaluacionRepository.First(u => u.Descripcion == request.Data.DetalleEvaluacion.Descripcion && u.IdDetalle != request.Data.DetalleEvaluacion.IdDetalle);
            if (existsDetalleEvaluacion != null)
                return ContractUtil.CreateInvalidResponse(new DetalleEvaluacionGetResponse { DetalleEvaluacion = request.Data.DetalleEvaluacion },
                        $"La descripción: {request.Data.DetalleEvaluacion.Descripcion} ya existe.");
            _uow.Commit();

            var model = request.Data.DetalleEvaluacion.ToDetalleEvaluacionModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _detalleEvaluacionRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new DetalleEvaluacionGetResponse
                {
                    DetalleEvaluacion = model.ToDetalleEvaluacionView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<DetalleEvaluacionGetResponse>(brokenRules);
        }
    }
}
