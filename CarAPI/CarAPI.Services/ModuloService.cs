﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Modulo;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class ModuloService : IModuloService
    {
        private readonly IModuloRepository _moduloRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<ModuloService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public ModuloService(IModuloRepository moduloRepository, ILogger<ModuloService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _moduloRepository = moduloRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<ModuloGetResponse> AddModulo(ContractRequest<ModuloAddRequest> request)
        {
            ContractResponse<ModuloGetResponse> response;

            try
            {
                var model = request.Data.Modulo.ToModuloModel(_mapper);
                model.IdModulo = Guid.NewGuid();

                var existsModulo = _moduloRepository.First(u => u.Descripcion == model.Descripcion);

                if (existsModulo != null)
                    return ContractUtil.CreateInvalidResponse(new ModuloGetResponse { Modulo = request.Data.Modulo },
                        $"La descripción: {request.Data.Modulo.Descripcion} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<ModuloGetResponse>(brokenRules);
                }
                else
                {
                    _moduloRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new ModuloGetResponse
                    {
                        Modulo = model.ToModuloView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<ModuloGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<ModuloGetResponse> GetModulo(ContractRequest<ModuloGetRequest> request)
        {
            ContractResponse<ModuloGetResponse> response;
            try
            {
                var model = _moduloRepository.First(u => u.IdModulo == request.Data.Id);
                var modelListResponse = new ModuloGetResponse
                {
                    Modulo = model.ToModuloView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<ModuloGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<ModuloGetListResponse> GetModulos(ContractRequest<ModuloGetRequest> request)
        {
            ContractResponse<ModuloGetListResponse> response;
            try
            {
                var models = _moduloRepository.GetAll();
                var modelListResponse = new ModuloGetListResponse
                {
                    Modulos = models.ToModuloViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<ModuloGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<ModuloGetResponse> UpdateModulo(ContractRequest<ModuloAddRequest> request)
        {
            var existsModulo = _moduloRepository.First(u => u.Descripcion == request.Data.Modulo.Descripcion && u.IdModulo != request.Data.Modulo.IdModulo);
            if (existsModulo != null)
                return ContractUtil.CreateInvalidResponse(new ModuloGetResponse { Modulo = request.Data.Modulo },
                        $"La descripción: {request.Data.Modulo.Descripcion} ya existe.");
            _uow.Commit();

            var model = request.Data.Modulo.ToModuloModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _moduloRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new ModuloGetResponse
                {
                    Modulo = model.ToModuloView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<ModuloGetResponse>(brokenRules);
        }
    }
}