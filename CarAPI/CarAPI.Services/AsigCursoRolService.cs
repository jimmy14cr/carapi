﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.AsigCursoRol;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class AsigCursoRolService : IAsigCursoRolService
    {
        private readonly IAsigCursoRolRepository _asigCursoRolRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<AsigCursoRolService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public AsigCursoRolService(IAsigCursoRolRepository asigCursoRolRepository, ILogger<AsigCursoRolService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _asigCursoRolRepository = asigCursoRolRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<AsigCursoRolGetResponse> AddAsigCursoRol(ContractRequest<AsigCursoRolAddRequest> request)
        {
            ContractResponse<AsigCursoRolGetResponse> response;

            try
            {
                var model = request.Data.AsigCursoRol.ToAsigCursoRolModel(_mapper);
                model.IdAsignacion = Guid.NewGuid();

                var existsAsigCursoRol = _asigCursoRolRepository.First(u => u.IdAsignacion == model.IdAsignacion);

                if (existsAsigCursoRol != null)
                    return ContractUtil.CreateInvalidResponse(new AsigCursoRolGetResponse { AsigCursoRol = request.Data.AsigCursoRol },
                        $"El ID: {request.Data.AsigCursoRol.IdAsignacion} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<AsigCursoRolGetResponse>(brokenRules);
                }
                else
                {
                    _asigCursoRolRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new AsigCursoRolGetResponse
                    {
                        AsigCursoRol = model.ToAsigCursoRolView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<AsigCursoRolGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<AsigCursoRolGetResponse> GetAsigCursoRol(ContractRequest<AsigCursoRolGetRequest> request)
        {
            ContractResponse<AsigCursoRolGetResponse> response;
            try
            {
                var model = _asigCursoRolRepository.First(u => u.IdAsignacion == request.Data.Id);
                var modelListResponse = new AsigCursoRolGetResponse
                {
                    AsigCursoRol = model.ToAsigCursoRolView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<AsigCursoRolGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<AsigCursoRolGetListResponse> GetAsigCursoRoles(ContractRequest<AsigCursoRolGetRequest> request)
        {
            ContractResponse<AsigCursoRolGetListResponse> response;
            try
            {
                var models = _asigCursoRolRepository.GetAll();
                var modelListResponse = new AsigCursoRolGetListResponse
                {
                    AsigCursoRoles = models.ToAsigCursoRolViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<AsigCursoRolGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<AsigCursoRolGetResponse> UpdateAsigCursoRol(ContractRequest<AsigCursoRolAddRequest> request)
        {
            var existsAsigCursoRol = _asigCursoRolRepository.First(u => u.IdAsignacion == request.Data.AsigCursoRol.IdAsignacion);
            if (existsAsigCursoRol != null)
                return ContractUtil.CreateInvalidResponse(new AsigCursoRolGetResponse { AsigCursoRol = request.Data.AsigCursoRol },
                        $"El ID: {request.Data.AsigCursoRol.IdAsignacion} ya existe.");
            _uow.Commit();

            var model = request.Data.AsigCursoRol.ToAsigCursoRolModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _asigCursoRolRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new AsigCursoRolGetResponse
                {
                    AsigCursoRol = model.ToAsigCursoRolView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<AsigCursoRolGetResponse>(brokenRules);
        }
    }
}