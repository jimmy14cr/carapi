﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Evaluacion;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class EvaluacionService : IEvaluacionService
    {
        private readonly IEvaluacionRepository _evaluacionRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<EvaluacionService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public EvaluacionService(IEvaluacionRepository evaluacionRepository, ILogger<EvaluacionService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _evaluacionRepository = evaluacionRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<EvaluacionGetResponse> AddEvaluacion(ContractRequest<EvaluacionAddRequest> request)
        {
            ContractResponse<EvaluacionGetResponse> response;

            try
            {
                var model = request.Data.Evaluacion.ToEvaluacionModel(_mapper);
                model.IdEvaluacion = Guid.NewGuid();

                var existsEvaluacion = _evaluacionRepository.First(u => u.IdEvaluacion == model.IdEvaluacion);

                if (existsEvaluacion != null)
                    return ContractUtil.CreateInvalidResponse(new EvaluacionGetResponse { Evaluacion = request.Data.Evaluacion },
                        $"EL ID: {request.Data.Evaluacion.IdEvaluacion} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<EvaluacionGetResponse>(brokenRules);
                }
                else
                {
                    _evaluacionRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new EvaluacionGetResponse
                    {
                        Evaluacion = model.ToEvaluacionView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<EvaluacionGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<EvaluacionGetResponse> GetEvaluacion(ContractRequest<EvaluacionGetRequest> request)
        {
            ContractResponse<EvaluacionGetResponse> response;
            try
            {
                var model = _evaluacionRepository.First(u => u.IdEvaluacion == request.Data.Id);
                var modelListResponse = new EvaluacionGetResponse
                {
                    Evaluacion = model.ToEvaluacionView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<EvaluacionGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<EvaluacionGetListResponse> GetEvaluaciones(ContractRequest<EvaluacionGetRequest> request)
        {
            ContractResponse<EvaluacionGetListResponse> response;
            try
            {
                var models = _evaluacionRepository.GetAll();
                var modelListResponse = new EvaluacionGetListResponse
                {
                    Evaluaciones = models.ToEvaluacionViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<EvaluacionGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<EvaluacionGetResponse> UpdateEvaluacion(ContractRequest<EvaluacionAddRequest> request)
        {
            var existsEvaluacion = _evaluacionRepository.First(u => u.IdEvaluacion == request.Data.Evaluacion.IdEvaluacion);
            if (existsEvaluacion != null)
                return ContractUtil.CreateInvalidResponse(new EvaluacionGetResponse { Evaluacion = request.Data.Evaluacion },
                        $"El ID: {request.Data.Evaluacion.IdEvaluacion} ya existe.");
            _uow.Commit();

            var model = request.Data.Evaluacion.ToEvaluacionModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _evaluacionRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new EvaluacionGetResponse
                {
                    Evaluacion = model.ToEvaluacionView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<EvaluacionGetResponse>(brokenRules);
        }
    }
}