﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Curso;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class CursoService : ICursoService
    {
        private readonly ICursoRepository _cursoRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<CursoService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public CursoService(ICursoRepository cursoRepository, ILogger<CursoService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _cursoRepository = cursoRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<CursoGetResponse> AddCurso(ContractRequest<CursoAddRequest> request)
        {
            ContractResponse<CursoGetResponse> response;

            try
            {
                var model = request.Data.Curso.ToCursoModel(_mapper);
                model.IdCurso = Guid.NewGuid();

                var existsCurso = _cursoRepository.First(u => u.NombreCurso == model.NombreCurso);

                if (existsCurso != null)
                    return ContractUtil.CreateInvalidResponse(new CursoGetResponse { Curso = request.Data.Curso },
                        $"El curso: {request.Data.Curso.NombreCurso} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<CursoGetResponse>(brokenRules);
                }
                else
                {
                    _cursoRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new CursoGetResponse
                    {
                        Curso = model.ToCursoView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<CursoGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<CursoGetResponse> GetCurso(ContractRequest<CursoGetRequest> request)
        {
            ContractResponse<CursoGetResponse> response;
            try
            {
                var model = _cursoRepository.First(u => u.IdCurso == request.Data.Id);
                var modelListResponse = new CursoGetResponse
                {
                    Curso = model.ToCursoView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<CursoGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<CursoGetListResponse> GetCursos(ContractRequest<CursoGetRequest> request)
        {
            ContractResponse<CursoGetListResponse> response;
            try
            {
                var models = _cursoRepository.GetAll();
                var modelListResponse = new CursoGetListResponse
                {
                    Cursos = models.ToCursoViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<CursoGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<CursoGetResponse> UpdateCurso(ContractRequest<CursoAddRequest> request)
        {
            var existsCurso = _cursoRepository.First(u => u.NombreCurso == request.Data.Curso.NombreCurso && u.IdCurso != request.Data.Curso.IdCurso);
            if (existsCurso != null)
                return ContractUtil.CreateInvalidResponse(new CursoGetResponse { Curso = request.Data.Curso },
                        $"El Curso: {request.Data.Curso.NombreCurso} ya existe.");
            _uow.Commit();

            var model = request.Data.Curso.ToCursoModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _cursoRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new CursoGetResponse
                {
                    Curso = model.ToCursoView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<CursoGetResponse>(brokenRules);
        }
    }
}
