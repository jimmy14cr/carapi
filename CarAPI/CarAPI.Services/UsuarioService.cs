﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Usuario;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<UsuarioService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public UsuarioService(IUsuarioRepository usuarioRepository, ILogger<UsuarioService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _usuarioRepository = usuarioRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<UsuarioGetResponse> AddUsuario(ContractRequest<UsuarioAddRequest> request)
        {
            ContractResponse<UsuarioGetResponse> response;

            try
            {
                var model = request.Data.Usuario.ToUsuarioModel(_mapper);
                model.IdUsuario = Guid.NewGuid();

                var existsUsuario = _usuarioRepository.First(u => u.Nombre == model.Nombre);

                if (existsUsuario != null)
                    return ContractUtil.CreateInvalidResponse(new UsuarioGetResponse { Usuario = request.Data.Usuario },
                        $"El nombre: {request.Data.Usuario.Nombre} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<UsuarioGetResponse>(brokenRules);
                }
                else
                {
                    _usuarioRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new UsuarioGetResponse
                    {
                        Usuario = model.ToUsuarioView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<UsuarioGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<UsuarioGetResponse> GetUsuario(ContractRequest<UsuarioGetRequest> request)
        {
            ContractResponse<UsuarioGetResponse> response;
            try
            {
                var model = _usuarioRepository.First(u => u.IdUsuario == request.Data.Id);
                var modelListResponse = new UsuarioGetResponse
                {
                    Usuario = model.ToUsuarioView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<UsuarioGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<UsuarioGetListResponse> GetUsuarios(ContractRequest<UsuarioGetRequest> request)
        {
            ContractResponse<UsuarioGetListResponse> response;
            try
            {
                var models = _usuarioRepository.GetAll();
                var modelListResponse = new UsuarioGetListResponse
                {
                    Usuarios = models.ToUsuarioViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<UsuarioGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<UsuarioGetResponse> UpdateUsuario(ContractRequest<UsuarioAddRequest> request)
        {
            var existsUsuario = _usuarioRepository.First(u => u.Nombre == request.Data.Usuario.Nombre && u.IdUsuario != request.Data.Usuario.IdUsuario);
            if (existsUsuario != null)
                return ContractUtil.CreateInvalidResponse(new UsuarioGetResponse { Usuario = request.Data.Usuario },
                        $"El usuario: {request.Data.Usuario.Nombre} ya existe.");
            _uow.Commit();

            var model = request.Data.Usuario.ToUsuarioModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _usuarioRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new UsuarioGetResponse
                {
                    Usuario = model.ToUsuarioView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<UsuarioGetResponse>(brokenRules);
        }
    }
}