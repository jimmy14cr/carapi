﻿using AutoMapper;
using CarAPI.Infrastructure.Messaging;
using CarAPI.Infrastructure.UnitOfWork;
using CarAPI.IRepositories;
using CarAPI.IServices;
using CarAPI.Services.Messaging.Communications.Leccion;
using CarAPI.Services.Messaging.Mapping;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarAPI.Services
{
    public class LeccionService : ILeccionService
    {
        private readonly ILeccionRepository _leccionRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILogger<LeccionService> _logger;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public LeccionService(ILeccionRepository leccionRepository, ILogger<LeccionService> logger, IUnitOfWork uow, ITimeZoneService timeZoneService, IMapper mapper)

        {
            _leccionRepository = leccionRepository;
            _mapper = mapper;
            _logger = logger;
            _timeZoneService = timeZoneService;
            _uow = uow;
        }

        public ContractResponse<LeccionGetResponse> AddLeccion(ContractRequest<LeccionAddRequest> request)
        {
            ContractResponse<LeccionGetResponse> response;

            try
            {
                var model = request.Data.Leccion.ToLeccionModel(_mapper);
                model.IdLeccion = Guid.NewGuid();

                var existsLeccion = _leccionRepository.First(u => u.Description == model.Description);

                if (existsLeccion != null)
                    return ContractUtil.CreateInvalidResponse(new LeccionGetResponse { Leccion = request.Data.Leccion },
                        $"La descripción: {request.Data.Leccion.Description} ya existe.");

                var brokenRules = model.GetBrokenRules().ToList();

                if (brokenRules.Any())
                {
                    response = ContractUtil.CreateInvalidResponse<LeccionGetResponse>(brokenRules);
                }
                else
                {
                    _leccionRepository.Add(model);
                    _uow.Commit();
                    var responseModel = new LeccionGetResponse
                    {
                        Leccion = model.ToLeccionView(_mapper)
                    };
                    response = ContractUtil.CreateResponse(responseModel);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<LeccionGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<LeccionGetResponse> GetLeccion(ContractRequest<LeccionGetRequest> request)
        {
            ContractResponse<LeccionGetResponse> response;
            try
            {
                var model = _leccionRepository.First(u => u.IdLeccion == request.Data.Id);
                var modelListResponse = new LeccionGetResponse
                {
                    Leccion = model.ToLeccionView(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<LeccionGetResponse>(ex);
            }

            return response;
        }


        public ContractResponse<LeccionGetListResponse> GetLecciones(ContractRequest<LeccionGetRequest> request)
        {
            ContractResponse<LeccionGetListResponse> response;
            try
            {
                var models = _leccionRepository.GetAll();
                var modelListResponse = new LeccionGetListResponse
                {
                    Lecciones = models.ToLeccionViewList(_mapper)
                };

                response = ContractUtil.CreateResponse(modelListResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                response = ContractUtil.CreateInvalidResponse<LeccionGetListResponse>(ex);
            }

            return response;
        }

        public ContractResponse<LeccionGetResponse> UpdateLeccion(ContractRequest<LeccionAddRequest> request)
        {
            var existsLeccion = _leccionRepository.First(u => u.Description == request.Data.Leccion.Description && u.IdLeccion != request.Data.Leccion.IdLeccion);
            if (existsLeccion != null)
                return ContractUtil.CreateInvalidResponse(new LeccionGetResponse { Leccion = request.Data.Leccion },
                        $"La descripción: {request.Data.Leccion.Description} ya existe.");
            _uow.Commit();

            var model = request.Data.Leccion.ToLeccionModel(_mapper);

            var brokenRules = model.GetBrokenRules().ToList();

            if (!brokenRules.Any())
            {
                _leccionRepository.Edit(model);

                //_uow.Context.Entry(model).Property(x => x.CreateDate).IsModified = false;

                _uow.Commit();

                var responseModel = new LeccionGetResponse
                {
                    Leccion = model.ToLeccionView(_mapper)
                };

                return ContractUtil.CreateResponse(responseModel);
            }

            return ContractUtil.CreateInvalidResponse<LeccionGetResponse>(brokenRules);
        }
    }
}